//
//  ViewController.swift
//  CheckCards
//
//  Created by joan mazo on 3/2/18.
//  Copyright © 2018 joan mazo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //var cardIOVC: CardIOPaymentViewController!
    var cardView: CardIOView!
    @IBOutlet weak var carditCardLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CardIOUtilities.preload()
        //setupScanner()
        setupCustomScannerView()
    }

    /*func setupScanner() {
        cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC.collectCardholderName = true
        cardIOVC.guideColor = UIColor.red
        cardIOVC.collectCVV = true
        cardIOVC.collectExpiry = true
    }*/
    
    func setupCustomScannerView() {
        cardView = CardIOView()
        cardView.translatesAutoresizingMaskIntoConstraints = false
        cardView.hideCardIOLogo = true
        cardView.scanExpiry = true
        cardView.useCardIOLogo = false
        cardView.delegate = self
        
        //try to change the border
        cardView.guideColor = UIColor.clear
        cardView.frame = view.frame
        cardView.contentMode = .scaleAspectFill
        cardView.backgroundColor = UIColor(white: 0, alpha: 0.6)
        
        view.addSubview(cardView)
    }
    
    @IBAction func showScannerCC(_ sender: Any) {
        view.addSubview(cardView)
    }
}

//CardIO custom view
extension ViewController: CardIOViewDelegate {
    func cardIOView(_ cardIOView: CardIOView!, didScanCard cardInfo: CardIOCreditCardInfo!) {
        if let info = cardInfo {
            print("credit card's info: ", info.cardNumber, info.expiryMonth, info.expiryYear, info.cardholderName)
            carditCardLabel.text = "credit card: \(info.cardNumber),  exp date: \(info.expiryYear, info.expiryMonth), name: \(info.cardholderName)"
        }
        
        cardView.removeFromSuperview()
    }
    
    
    
}

// MARK: cardIO: ViewController
/*extension ViewController: CardIOPaymentViewControllerDelegate {
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        
        if let info = cardInfo {
            print("This is the card Info: ", info.cardholderName, info.cardNumber, info.redactedCardNumber, info.expiryYear, info.expiryMonth, info.cvv)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}*/














